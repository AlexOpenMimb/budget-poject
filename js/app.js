const ingress = [];

const egress = [];

/* console.log(ingress[0]);
console.log(ingress[1]); */

//document.getElementById("body").addEventListener("load", chargeApp);

const chargeApp = () => {
  chargeHeader();
  chargeIngress();
  chargeEgress();
};

const totalIngress = () => {
  let totalIngres = 0;
  for (let ingres of ingress) {
    totalIngres += ingres.value;
  }
  return totalIngres;
};

const totalEgress = () => {
  let totalEgres = 0;
  for (let egres of egress) {
    totalEgres += egres.value;
  }
  return totalEgres;
};

let chargeHeader = () => {
  let budget = totalIngress() - totalEgress();
  let egressPercentage = totalEgress() / totalIngress();
  if (isNaN(egressPercentage)) egressPercentage = " ";
  document.getElementById("budget").innerHTML = currencyFormat(budget);
  document.getElementById("percentage_2").innerHTML =
    pecentageFormat(egressPercentage);
  document.getElementById("ingress").innerHTML = currencyFormat(totalIngress());
  document.getElementById("egress").innerHTML = currencyFormat(totalEgress());
};

const currencyFormat = (value) => {
  return value.toLocaleString("en-US", {
    style: "currency",
    currency: "USD",
    minimumFractionDigits: 0,
  });
};

const pecentageFormat = (value_1) => {
  return value_1.toLocaleString("eN-US", {
    style: "percent",
    minimumFractionDigits: 1,
  });
};

const chargeIngress = () => {
  let ingressHTML = " ";
  for (let ingreso of ingress) {
    ingressHTML += createIngress(ingreso);
  }
  document.getElementById("acount-box__adder-ingress").innerHTML = ingressHTML;
};

const createIngress = (ingres) => {
  let ingressHTML = `
         <div class="acount-box__list">

                    <div class="aconunt-box__description">${
                      ingres.description
                    }</div>
                    <div class="acount-box__rigt">
                        <div class="aconunt-box__value-ingress">${currencyFormat(
                          ingres.value
                        )}
                        </div>
                        <div class=acount-box__deleted>
                            <button class="aconunt-box__icon aconunt-box__icon--blue"><span class="icon-cancel-circled" onclick ='deletedIngress(${
                              ingress.id
                            })' ></span>
                            </button>                        
                        </div>                    
                    </div>
                </div>

  `;
  return ingressHTML;
};

const deletedIngress = (id) => {
  let deletedIndex = ingress.findIndex((ingress) => ingress.id === id);
  ingress.splice(deletedIndex, 1);
  chargeHeader();
  chargeIngress();
};

const chargeEgress = () => {
  let egressHTML = " ";
  for (let egres of egress) {
    egressHTML += createEgress(egres);
  }
  document.getElementById("acount-box__adder-egress").innerHTML = egressHTML;
};

const createEgress = (egres) => {
  let egressHTML = `
      <div class="acount-box__list">
                    <div class="aconunt-box__description">${
                      egres.description
                    }</div>
                    <div class="acount-box__rigt">
                        <div class="acount-box__value-egress">${currencyFormat(
                          egres.value
                        )}
                        </div>
                        <div class="acount-box__percentage">${pecentageFormat(
                          egres.value / totalIngress()
                        )}</div>
                        <div class=acount-box__deleted>
                            <button class="aconunt-box__icon aconunt-box__icon--red"><span class="icon-cancel-circled"  onclick ='deletedEgress(${
                              egress.id
                            })' ></span>
                            </button>                        
                        </div>                    
                    </div>
                </div>
  `;
  return egressHTML;
};

let p = egress.value / ingress.value;

const deletedEgress = (id) => {
  let deletedIndex = egress.findIndex((egress) => egress.id === id);
  egress.splice(deletedIndex, 1);
  chargeHeader();
  chargeEgress();
};

const addDate = () => {
  let form = document.forms["form"];
  let type = form["type"];
  let description = form["description"];
  let value_2 = form["value"];
  if (description.value !== " " && value_2.value !== " ") {
    if (type.value === "ingress") {
      ingress.push(new Ingress(description.value, +value_2.value));
      chargeApp();
      chargeIngress();
    } else if (type.value === "egress") {
      egress.push(new Egress(description.value, +value_2.value));
      chargeApp();
      chargeEgress();
    }
  }
};
