class Egress extends Dato {
  static accountantEgress = 0;
  constructor(description, value) {
    super(description, value);
    this._id = ++Egress.accountantEgress;
  }
  get id() {
    return this._id;
  }
}
